======
s2_add
======


Simple Webservice that adds two band from Sentinel-2 data.


Description
===========


Installation
============

Download miniconda and create a new conda environment using the provided
``conda_environment.yml``.

.. code::
   conda env create -f conda_environment.yml

Running
=======

To run the service in debug mode edit the ``start_s2_add`` script. Especially
the S2_ADD_DATA variable has to be set to point to a folder containing S2 data
in SAFE format.

Requests
========

Basic
-----

- ``GET http://127.0.0.1:5000/data``: Returns a list of the available datasets
- ``GET http://127.0.0.1:5000/process``: Returns a list of available processes
- ``GET http://127.0.0.1:5000/data/S2A_MSIL1C_20170210T100131_N0204_R122_T33UVP_20170210T100132/red``: Get red band of the product. Default response is a png image at the moment.


Processes
---------

addition
~~~~~~~~

.. code::

    POST http://127.0.0.1:5000/process/addition/
    Content-Type: application/json
    
    {
            "band1": "/data/S2A_MSIL1C_20170210T100131_N0204_R122_T33UVP_20170210T100132/red",
            "band2": "/data/S2A_MSIL1C_20170210T100131_N0204_R122_T33UVP_20170210T100132/green"
    }

Adds the two products and returns a png image.



Note
====

This project has been set up using PyScaffold 2.5.7. For details and usage
information on PyScaffold see http://pyscaffold.readthedocs.org/.
