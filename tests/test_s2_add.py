from s2_add.s2_add import decode_product_id

def test_decode_product_id():

    product_id ='/data/S2A_MSIL1C_20170210T100131_N0204_R122_T33UVP_20170210T100132/red'
    data_id_calc, band_name_calc = decode_product_id(product_id)
    assert data_id_calc == 'S2A_MSIL1C_20170210T100131_N0204_R122_T33UVP_20170210T100132'
    assert band_name_calc == 'red'
