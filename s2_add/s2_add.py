from flask import Flask
from flask import make_response
from flask import request
import glob
import os
import StringIO
from osgeo import gdal
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
app = Flask(__name__)


def get_data_folder():
    return os.environ['S2_ADD_DATA']

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/data')
def data():
    flist = glob.glob(os.path.join(get_data_folder(), "*.SAFE"))
    return "".join(['<p>{}</p>'.format(x) for x in flist])

@app.route('/process')
def get_process_list():
    return "addition"

@app.route('/process/<process_name>/',  methods=['POST', 'GET'])
def call_process(process_name):
    lut = {'addition': addition_http_response}
    if request.method == 'POST':
        content = request.json
        product1, band1 = decode_product_id(content['band1'])
        product2, band2 = decode_product_id(content['band2'])
        return lut[process_name](product1, band1, product2, band2)
    else:
        return "band1, band2 are needed parameters"

@app.route('/data/<ident>/<band>')
def data_png_response(ident, band):
    data = get_s2_data(ident, band)
    return png_plot_http_response(data)

def decode_product_id(product_id):
    """
    Decode product identifier into data identifier and band name.

    Parameters
    ----------
    product_id: string
        something like
        /data/S2A_MSIL1C_20170210T100131_N0204_R122_T33UVP_20170210T100132/red

    Returns
    -------
    data_id: string
        The data identifier
        e.g. S2A_MSIL1C_20170210T100131_N0204_R122_T33UVP_20170210T100132
    band_name: string
        e.g. red
    """
    parts = product_id.split('/')
    data_id = parts[2]
    band_name = parts[-1]
    return data_id, band_name

def addition_http_response(product1, band1, product2, band2):
    result = addition(product1, band1,
                      product2, band2)
    return png_plot_http_response(result)

def png_plot_http_response(data):
    """
    Plots the given input data into png http response.

    Parameters
    ----------
    data: numpy.ndarray
        input data

    Returns
    -------
    response: flask HTTPResponse
    """
    fig=Figure()
    ax=fig.add_subplot(111)
    ax.imshow(data)
    canvas=FigureCanvas(fig)
    png_output = StringIO.StringIO()
    canvas.print_png(png_output)
    response=make_response(png_output.getvalue())
    response.headers['Content-Type'] = 'image/png'
    return response


def addition(product1, band1, product2, band2):
    data1 = get_s2_data(product1, band1)
    data2 = get_s2_data(product2, band2)
    result = data1 + data2
    return result

def get_s2_data(ident, band):
    """
    Read the Sentinel-2 Band

    Parameters
    ----------
    ident: string
        Sentinel-2 identifier,
        filename without zip or SAFE extension
    band: string
        one of green, red or blue
    """
    fname = os.path.join(get_data_folder(), ident + '.SAFE', 'MTD_MSIL1C.xml')
    ds = gdal.Open(fname)
    subdatasets = ds.GetSubDatasets()
    subds_name = subdatasets[0][0]
    subds = gdal.Open(subds_name)
    band_lookup = {'green': 2,
                   'red': 1,
                   'blue': 3}
    band = subds.GetRasterBand(band_lookup[band])
    data = band.ReadAsArray(0,0,100,100)
    ds = None
    subds = None
    return data
